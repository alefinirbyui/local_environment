# A Demo Static Site using Vagrant, PHP and Nginx

This repo contains the code for building a PHP website using an Nginx server. The code for the site is saved in `./www/` folder, and the server config is in `./Vagrantfile` and `./bootstrap.sh`.

To get the environment running, run the following command from inside the project directory:

```sh
$ Vagrantup
```

If you visit `http://localhost:8080` in your browser, you should be able to see the website runnning.